//
//  QuestionGenerator.h
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/14/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Question;

NS_ASSUME_NONNULL_BEGIN
@interface QuestionGenerator : NSObject


- (void) generateQuestions:(NSInteger) page:(void (^)(NSArray<Question *> *, NSError * _Nullable error))completionHandler ;

@end
NS_ASSUME_NONNULL_END
