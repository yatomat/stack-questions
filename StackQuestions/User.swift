//
//  User.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/30/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

import Foundation

struct Badges: Codable {
    let gold: Int
    let silver: Int
    let bronze: Int
}

struct User : Codable {
    let name: String
    let image: String
    let reputation: Int?
    let badges: Badges
    
    private enum CodingKeys : String, CodingKey {
        case name="display_name", image = "profile_image", reputation, badges = "badge_counts"
    }

}
