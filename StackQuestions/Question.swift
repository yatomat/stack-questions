//
//  Question.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/13/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

import UIKit

@objc class Question : NSObject, Decodable {
        
    @objc public var name: String?
    @objc public var photo: String?
    @objc public var title: String?
    @objc public var userId: String?
}
