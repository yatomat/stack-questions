//
//  QuestionFetcher.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/30/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//
class QuestionFetcher {
    public typealias QuestionFetcherCallback = (Bool, Data?) -> Void
    
    class func load(URL: URL, taskCallback: @escaping QuestionFetcherCallback) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            let statusCode = (response as! HTTPURLResponse).statusCode
            if (error == nil && statusCode == 200) {
                taskCallback(true, data)
            } else {
                if(error != nil){
                    print("Failure: %@", error!.localizedDescription);
                }
                taskCallback(true, nil)
            }
        })
        task.resume()
    }
}
