//
//  QuestionGenerator.m
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/14/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

#import "QuestionGenerator.h"
#import <StackQuestions-Swift.h>
#import "QuestionBuilder.h"

@implementation QuestionGenerator

- (void) generateQuestions:(NSInteger) page :(void (^)(NSArray<Question *> *, NSError * _Nullable error))completionHandler {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSString *urlAsString = [NSString stringWithFormat:@"https://api.stackexchange.com/2.2/questions?site=stackoverflow&sort=creation&order=asc&page=%lu", page];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    if(url){
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (!data || httpResponse.statusCode != 200){
                NSLog(@"Error %@", [error localizedDescription]);
                
                if(error){
                    NSError *generatedError = [self generateError];
                    completionHandler(array, generatedError);
                }else{
                    completionHandler(array, error);
                }
                return;
            }
            else {
                [array addObjectsFromArray:[QuestionBuilder questionsFromJSON:data error:&error]];
                completionHandler(array, error);
            }
        }];
        [dataTask resume];
    } else {
        NSError *generatedError = [self generateError];
        completionHandler(array, generatedError);
    }
}

- (NSError*) generateError {
    NSError *error = [NSError errorWithDomain:@"Something is not right with request"
                                         code:-101
                                     userInfo:[NSDictionary dictionaryWithObject:@"Something is not right with request" forKey:NSLocalizedDescriptionKey]];
    return error;
}

@end
