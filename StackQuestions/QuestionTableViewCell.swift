//
//  QuestionTableViewCell.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/13/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var avatartImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatartImageView.image = nil
    }

}
