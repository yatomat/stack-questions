//
//  QuestionTableViewController.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/13/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


class QuestionTableViewController: UITableViewController {
    
    var questions = [Question]()
    
    var cachedImages = [String: UIImage]()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        questions = [];
        loadSampleQuestions();
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSampleQuestions()
        self.tableView.tableHeaderView = UIView(frame: CGRect.zero)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cellUnique"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? QuestionTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        
        if(questions.count>0){
            let question = questions[indexPath.row]
            
            cell.name.text = question.name
            if let photoItem=question.photo {
                if cachedImages[photoItem] != nil {
                    cell.avatartImageView.image = cachedImages[photoItem]
                }
                else {
                    downloadImage(link: photoItem, callback: { image in
                        if let image = image {
                            self.cachedImages[photoItem] = image
                            cell.avatartImageView.image = image
                        }
                    })
                }
            }
            cell.title.text = question.title
            
            //handle pagination
            
            if indexPath.row == questions.count - 1 { // last cell
                loadSampleQuestions()
            }
        }
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender : Any?) {
        if (segue.identifier == "showQuestionDetails") {
            if let questionDetailsViewController = segue.destination as? QuestionDetailsViewController{
                let selectedIndex = self.tableView.indexPath(for: sender as! UITableViewCell)!
                questionDetailsViewController.questionDetails = questions[selectedIndex.item]
            }
        }
    }
    
    @IBAction func refreshTableFunc() {
        questions = []
        self.tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
            self.loadSampleQuestions()
            self.refreshControl?.endRefreshing();
        }
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: { _ in
            toastLabel.removeFromSuperview()
        })
    }
    
    private func loadSampleQuestions() {
        let questionGenerator = QuestionGenerator()
        let page =  round(Double(questions.count/10) / 100) * 100
        questionGenerator.generateQuestions(Int(page+1), { [weak self]generatedQuestions, error in
            if let errorFromGenerator = error {
                DispatchQueue.main.async {
                    self?.showToast(message: errorFromGenerator.localizedDescription)
                }
            }
            else {
                self?.questions.append(contentsOf: generatedQuestions)
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                    //                    self?.tableView.beginUpdates()
                }
                
            }
        })
    }
    
    typealias DowbloadImageCallback = (UIImage?) -> ()
    private func downloadImage(link: String, callback: @escaping DowbloadImageCallback) {
        guard let url = URL(string: link) else { return }
        let task = URLSession.shared.dataTask(with: url) { responseData, responseUrl, error in
            if let data = responseData {
                DispatchQueue.main.async() {
                    callback(UIImage(data: data))
                }
            }
            else {
                DispatchQueue.main.async() {
                    callback(nil)
                }
            }
        }
        
        task.resume()
    }
    
}
