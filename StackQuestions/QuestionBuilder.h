//
//  QuestionBuilder.h
//  StackQuestions
//
//  Created by Sergey Kryutsin on 4/2/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionBuilder : NSObject

+ (NSArray *)questionsFromJSON:(NSData *)objectNotation error:(NSError **)error;

@end
