//
//  UserResponse.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 4/3/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

import Foundation

struct UserResponse : Decodable {
    public let items : [User]
}
