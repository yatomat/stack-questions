//
//  QuestionDetailsViewController.swift
//  StackQuestions
//
//  Created by Sergey Kryutsin on 3/29/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//
import UIKit

class QuestionDetailsViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    var questionDetails: Question?
    var user: User? {
        didSet {
            
        }
    }
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var reputationLabel: UILabel!
    
    fileprivate func showError() {
        let alert = UIAlertController.init(title: "No network connection", message: "Please check it", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = questionDetails?.name
        if(questionDetails?.userId == nil){
            reputationLabel.text = "User is not exited anymore"
        } else {
            let url = URL(string: String(format: "https://api.stackexchange.com/2.2/users/%@?site=stackoverflow", questionDetails!.userId!))!
            QuestionFetcher.load(URL:url,
                                 taskCallback:{[weak self] success, result in
                                    
                                    if let strongSelf = self{
                                        if(success){
                                            do {
                                                let users = try JSONDecoder().decode(UserResponse.self, from: result!)
                                                DispatchQueue.main.async {
                                                    strongSelf.user = users.items.first //users.items[0]
                                                    if let currentUser = strongSelf.user {
                                                        let badges = currentUser.badges
                                                        let totalCount = badges.gold + badges.silver + badges.bronze
                                                        strongSelf.reputationLabel.text = String(currentUser.reputation ?? 0)
                                                        strongSelf.nameLabel.text = String(currentUser.name)
                                                        strongSelf.countLabel.text = String(totalCount)
                                                        strongSelf.userAvatar.downloadedFrom(link: currentUser.image)
                                                    } else {
                                                        strongSelf.showError()
                                                    }
                                                }
                                            } catch {
                                                print("Error deserializing JSON: \(error)")
                                            }
                                        } else {
                                            DispatchQueue.main.async {
                                                strongSelf.showError()
                                            }
                                        }
                                    }
            })
        }
        
    }
}
