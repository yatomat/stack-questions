//
//  QuestionBuilder.m
//  StackQuestions
//
//  Created by Sergey Kryutsin on 4/2/18.
//  Copyright © 2018 Sergey Kryutsin. All rights reserved.
//

#import "QuestionBuilder.h"
#import <StackQuestions-Swift.h>

@implementation QuestionBuilder
+ (NSArray *)questionsFromJSON:(NSData *)objectNotation error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *questions = [[NSMutableArray alloc] init];
    
    NSArray *results = [parsedObject valueForKey:@"items"];
    
    for (NSDictionary *questionDic in results) {
        Question *question = [Question new];
        NSDictionary *ownerDic = questionDic[@"owner"];
        if([ownerDic[@"display_name"] isKindOfClass:[NSString class]]){
            [question setName:ownerDic[@"display_name"]];
        }
        if([ownerDic[@"display_name"] isKindOfClass:[NSString class]]){
            [question setTitle:questionDic[@"title"]];
        }
        if([ownerDic[@"display_name"] isKindOfClass:[NSString class]]){
            [question setPhoto:ownerDic [@"profile_image"]];
        }
        if([ownerDic objectForKey:@"user_id"] && [ownerDic[ @"user_id"] isKindOfClass:[NSNumber class]]){
            NSNumber *userId =ownerDic[@"user_id"];
            [question setUserId: userId.stringValue];
        }
        [questions addObject:question];
    }
    
    return questions;
}

@end
